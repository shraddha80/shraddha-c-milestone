#include<stdio.h>
int cal_n(int n)
{
   if (n <= 1)
      return 1;
   return n*cal_n(n-1);
}

int nPr(int n, int r)
{
   return cal_n(n)/cal_n(n-r);
}
int main()
{
   int n=5, r=2;
   printf("value of %dP%d is %d", n, r, nPr(n, r));
   return 0;
}