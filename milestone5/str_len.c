#include <stdio.h>
int str_length(char *str);
int main()
{
    char s[]="Welcome to Terna Engineering College";
    int ans;
    ans=str_length(s);
    printf("%d",ans);
    return 0;
}
int str_length(char *str)
{
    int len;
    len=strlen(str);
    if(len!=0)
        return len;
    else
        return -1;
}